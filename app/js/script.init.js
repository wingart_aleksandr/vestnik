//  /*================================================>
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/





		var mask	   	= $('input[type="tel"]'),
		    styler 		= $(".styler"),
		    select 		= $(".styler2"),
		    owl 		= $(".owl-carousel"),
		    swipe 		= $('.js-swipe>div'),
		    validate   	= $('.validate'),
			mapYa		= $('.ymap');


			
			if(swipe.length){
					include('plugins/swipe/jquery.mobile.custom.min.js');
			}
			if(owl.length){
					include('plugins/owl-carousel/owl.carousel.min.js');
					includeCss("plugins/owl-carousel/owl.carousel.min.css");
			}
			if(validate.length){
					include("plugins/jquery.validate/jquery.validate.min.js");
			}
			if(styler.length){
		 		includeCss("plugins/formstyler/jquery.formstyler.css");
		 		includeCss("plugins/formstyler/jquery.formstyler.theme.css");
		 		include("plugins/formstyler/jquery.formstyler.js");
			}
			if(select.length){
		 		includeCss("plugins/select/select2.min.css");
		 		include("plugins/select/select2.min.js");
			}
			if(mask.length){
					include("plugins/inputmask/inputmask.js");
					include("plugins/inputmask/inputmask.phone.extensions.js");
					include("plugins/inputmask/jquery.inputmask.js");
			}
			if(mapYa.length){
					include("https://api-maps.yandex.ru/2.1/?lang=ru_RU");
			}

			// 		include("plugins/modernizr.js");



			function include(url){

					document.write('<script src="'+ url + '"></script>');

			}

			function includeCss(href){

					var href = '<link rel="stylesheet" media="screen" href="' + href +'">';

					if($("[href*='style.css']").length){

						$("[href*='style.css']").before(href);

					}
					else{

						$("head").prepend(href);

					}

			}




		$(document).ready(function(){



			/* ------------------------------------------------
			SWipe START
			------------------------------------------------ */

					if(swipe.length){

						if($(window).width() <= 767){

							swipe.on( "swipeleft", swipeleft );
							swipe.on( "swiperight", swiperight );
							  
							function swipeleft( event ){
								if($(this).next('div').length){
									$(this).parent().addClass( "swipeleft" ).removeClass('swiperight');
									$(this).hide().next('div').addClass('active').show().siblings().removeClass('active');

									var index = $(this).parent().find('div.active').index();

									$(this).parent().next('.js-tabs-dots').children('div').eq(index).addClass('active').siblings('div').removeClass('active');
								}
							}

							function swiperight( event ){
								if($(this).prev('div').length){
									$(this).parent().addClass( "swiperight" ).removeClass('swipeleft');
									$(this).hide().prev('div').addClass('active').show().siblings().removeClass('active');

									var index = $(this).parent().find('div.active').index();

									$(this).parent().next('.js-tabs-dots').children('div').eq(index).addClass('active').siblings('div').removeClass('active');
								}
							}

						}
					}

			/* ------------------------------------------------
			SWipe END
			------------------------------------------------ */

			/* ------------------------------------------------
			Inputmask START
			------------------------------------------------ */

					if(mask.length){

						mask.inputmask({
							"mask": "+7 (999) 999-9999",
							'showMaskOnHover': false,
							"clearIncomplete": true,
							'oncomplete': function(){
								// console.log('Ввод завершен');
							},
							'onincomplete': function(){
								// console.log('Заполнено не до конца');
							},
							'oncleared': function(){
								// console.log('Очищено');
							}
						});

					}

			/* ------------------------------------------------
			Inputmask END
			------------------------------------------------ */

			/* ------------------------------------------------
			FORMSTYLER START
			------------------------------------------------ */

					if (styler.length){
						
						styler.styler({
							selectSmartPositioning: true
						});
					}

			/* ------------------------------------------------
			FORMSTYLER END
			------------------------------------------------ */

			/* ------------------------------------------------
			SELECT START
			------------------------------------------------ */

					if (select.length){
						
						select.select2();
						
					}

			/* ------------------------------------------------
			SELECT END
			------------------------------------------------ */

			/* ------------------------------------------------
			Validate START
			------------------------------------------------ */

					if(validate.length){

						$(validate).each(function(index,el){
		                    // var id = $(el).attr("id");
		                    var id = $(el);

		                    id.validate({

								rules:{
									// first
									cf_select_opf: {
										required: true	
									},
									cf_textarea_opf: {
										required: true,
										minlength: 3
									},
									cf_input_opf: {
										required: true,
										minlength: 2
									},
									cf_input_ogrn: {
										required: true,
										minlength: 13
									},
									cf_input_inn: {
										required: true,
										minlength: 12
									},
									cf_input_kpp: {
										required: true,
										minlength: 9
									},
									cf_textarea_adress: {
										required: true,
										minlength: 3
									},

									// second
									cf_select_decision: {
										required: true	
									},
									cf_select_doc: {
										required: true	
									},
									cf_input_number: {
										required: true,
										number: true,
										minlength: 2
									},
									cf_input_data: {
										required: true,
										minlength: 2
									},

									//third
									cf_textarea_credit_adress: {
										required: true,
										minlength: 3
									},
									// forth
									cf_select_applicant: {
										required: true	
									},
									cf_input_fullname:{
										required: true,
										minlength: 2
									},
									cf_input_number_seria:{
										required: true,
										number: true,
										minlength: 2
									},
									cf_textarea_extradition:{
										required: true,
										minlength: 3
									}

								},

								messages:{
									// first
									cf_select_opf: {
										required: "Поле обязательно для заполнения"
									},
									cf_textarea_opf: {
										required: "Поле обязательно для заполнения",
										minlength: "Не достаточно символов"
									},
									cf_input_opf: {
										required: "Поле обязательно для заполнения",
										minlength: "Не достаточно символов"
									},
									cf_input_ogrn: {
										required: "Поле обязательно для заполнения",
										minlength: "Не достаточно символов"
									},
									cf_input_inn: {
										required: "Поле обязательно для заполнения",
										minlength: "Не достаточно символов"
									},
									cf_input_kpp: {
										required: "Поле обязательно для заполнения",
										minlength: "Не достаточно символов"
									},
									cf_textarea_adress: {
										required: "Поле обязательно для заполнения",
										minlength: "Не достаточно символов"
									},

									// second
									cf_select_decision: {
										required: "Поле обязательно для заполнения"
									},
									cf_select_doc: {
										required: "Поле обязательно для заполнения"
									},
									cf_input_number: {
										required: "Поле обязательно для заполнения",
										number: "Некорректное значение поля",
										minlength: "Не достаточно символов"
									},
									cf_input_data: {
										required: "Поле обязательно для заполнения",
										minlength: "Поле обязательно для заполнения"
									},

									//third
									cf_textarea_credit_adress: {
										required: "Поле обязательно для заполнения",
										minlength: "Не достаточно символов"
									},
									// forth
									cf_select_applicant: {
										required: "Поле обязательно для заполнения"
									},
									cf_input_fullname:{
										required: "Поле обязательно для заполнения",
										minlength: "Не достаточно символов"
									},
									cf_input_number_seria:{
										required: "Поле обязательно для заполнения",
										number: "Некорректное значение поля",
										minlength: "Не достаточно символов"
									},
									cf_textarea_extradition:{
										required: "Поле обязательно для заполнения",
										minlength: "Не достаточно символов"
									}

								},

								invalidHandler: function() {

							        setTimeout(function() {
							            $('.styler').trigger('refresh');
							        }, 1000)

							    }

							});
		                });

					}

			/* ------------------------------------------------
			Validate END
			------------------------------------------------ */


			/* ------------------------------------------------
			OWL START
			------------------------------------------------ */

					if(owl.length){

					$(".carousel_box").owlCarousel({
						items : 1,
						// loop: true,
						smartSpeed:1000,
						nav: true,
			            navText: [ '', '' ],
			            responsive:{
					        0:{
					            items:1,
					            nav:true
					        },

					        768:{
					            items:2,

					        },
					        1200:{
					            items:1,
					        }
					    }
					});
				}
			/* ------------------------------------------------
			OWL END
			------------------------------------------------ */




		});


		$(window).load(function(){



		});




//  /*================================================>
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
