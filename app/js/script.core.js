;(function($){

	"use strict";

	var Core = {

		DOMReady: function(){

			var self = this;

			self.openCloseMenu();
			self.tabs();
			self.accordion();
			self.close();
			self.interactiveForm.init();
			self.stickyField.init();
			self.interactiveMap.init();
		},

		windowLoad: function(){

			var self = this;

			self.preloader();

			if($('.ymap').length){

				self.mapYa();

			}

		},

		/**
		**	stickyField
		**/

		stickyField: {
			init: function(){
				
				var self = this;

					self.window = $(window);
					self.public = $('.public_container');
					self.stickyBlockH = self.window.height();
					self.stickyBlock = $('.public_wrap');
					self.stickyBlockW = self.stickyBlock.width();

				if(self.window.width() <= 479){

					self.stickyBlock.children().wrap('<div class="stickyWrap"></div>');
					
					// self.public.addClass('hideEl').find('.form_row').slideUp(300,function(){
					// 	self.stickyBlock.css({
					// 		height: self.public.outerHeight()
					// 	});
					// });

					self.tabletHeightBlock();
					self.sticked();
					self.adsClass();

					self.window.on('scroll resize', function(){
						self.sticked();
						self.tabletHeightBlock();
					});
					
				}

			},

			sticked: function(){
				
				var self = this;

					self.stickyBlockWidth = self.stickyBlock.width();
					self.stickyBlockOffset = self.stickyBlock.offset().top;
					self.windowOff = self.window.scrollTop();


				$('.stickyWrap').css({
					width: self.stickyBlockWidth
				});

				if(self.windowOff  >= self.stickyBlockOffset){
					self.stickyBlock.addClass('sticky');
					self.public.addClass('hideEl').find('.form_row').slideUp(600,function(){
						self.stickyBlock.css({
							height: self.public.outerHeight()
						});
					});
				}

				else{
					self.stickyBlock.removeClass('sticky');
					self.public.removeClass('hideEl').find('.form_row').slideDown(800,function(){
						self.stickyBlock.css({
							height: self.public.outerHeight()
						});
					});
				}

			},

			tabletHeightBlock: function(){

				var self = this;

				self.publicIndent = self.public.outerHeight() - self.public.height(); 
				self.publicItem = $('.public_container_item').outerHeight();

				if(self.window.width() <= 479 && !self.public.hasClass('mobile')){
					
					self.stickyBlock.css({
						height: self.window.height() - 100
					});
					$('.stickyWrap').css({
						width: self.stickyBlockWidth
					});
					self.public.find('.form_textarea').css({
						'min-height': self.stickyBlock.height() - self.publicIndent - self.publicItem
					});
					
					// public_mobile 
					self.public.addClass('mobile');
					
				}
				else if(self.window.width() >= 480 && self.public.hasClass('mobile')){
					// public_mobile 
					self.public.removeClass('mobile')
				}

			},

			adsClass: function(){

				var self = this;
				$('.public_title').on('click', function(event) {
					$(this).closest('.public_container').toggleClass('hideEl').find('.form_row').slideToggle(600,function(){
						if(!$('.public_wrap').hasClass('sticky')){
							self.stickyBlock.css({
								height: self.public.outerHeight()
							});
						}
					});
				});
			} 


		},

		/**
		**	interactiveForm
		**/

		interactiveForm: {

			init: function(){

				var self = this;
					self.forma = $('.first_form');
					self.ckeckForm = $('.ckeckForm');
					self.checkToggle = $('#toggle-checkbox');
					self.sel = $('.js-publication-select');
					self.sel2 = $('.js-accounting-select');
					self.sel3 = $('.js-accounting-select2');
					self.sel4 = $('.js-publication-select2');
					self.sel5 = $('.js-select');
					self.sel6 = $('.js-select1');
					self.sel7 = $('.js-select2');
					self.checkboxPublic = $('.js-check-public');
					self.afforgArr = [];

				self.methodChekedForm();
				self.formattingField();
				self.showHideForm();
				self.btnSteps();
				self.selectChange();
				self.checkboxEvent();
				self.events();
			},

			methodChekedForm: function(){
				
				var self = this;

				self.ckeckForm.on('click', function(event) {
					
					var $this = $(this);
					
					if($this.attr('data-toggle-check') == "off"){
						self.checkToggle.removeClass('active').removeAttr('checked').prop('checked', false);
						self.changeForm('false');
					}
					if($this.attr('data-toggle-check') == 'on'){
						self.checkToggle.addClass('active').prop('checked',true);
						self.changeForm('true');
					}
				});

				self.checkToggle.on('click', function(event) {
					
					var $this = $(this);
					
					if(!$this.is(':checked')){
				        $this.removeClass('active').removeAttr('checked');
				        self.changeForm('false');
				    } 
				    else {
				        $this.addClass('active').attr('checked','checked');
				        self.changeForm('true');
				    }

				});

			},

			changeForm: function(check){
				
				var self = this;

				if(check == 'true'){
					self.forma.addClass('steps_form');
					$('.step_box.form_wrap').addClass('active');
					self.checkBtns();
				}
				if(check == 'false'){
					self.forma.removeClass('steps_form');
					$('.step_box.form_wrap').removeClass('step_box active');
					$('.first_step.form_wrap').addClass('step_box');
					self.checkPublication();
				}

			},

			formattingField: function(){

				var self = this;

				$('.formatting_label,.jq-checkbox.formatting').on('click', function(event) {
					
					if($('.jq-checkbox.formatting').hasClass('checked')){
						$('.jq-checkbox.formatting').find('input').attr('checked', true).addClass('checked').closest('.public_container').find('textarea').attr('readonly', true);
					}
					else{
						$('.jq-checkbox.formatting').find('input').attr('checked', false).removeClass('checked').closest('.public_container').find('textarea').attr('readonly', false);
					}

				});
				if($('.jq-checkbox.formatting').hasClass('checked')){
					$('.jq-checkbox.formatting').find('input').attr('checked', true).addClass('checked').closest('.public_container').find('textarea').attr('readonly', true);
				}

			},

			showHideForm: function(){

				var self = this;

				$('.js_use_electronic').on('click', function(event) {
					
					if($(this).hasClass('checked')){
						$('.js-regionals').addClass('form_hide').animate({
							opacity : 0,
							visibility : 'hidden',
						},
						{
							duration: 500, 
							complete: function() {
								$(this).slideUp(800);
							}
						});
					}

				});
				$('.js_no_use_electronic').on('click', function(event) {
					
					if($(this).hasClass('checked')){
						$('.js-regionals').removeClass('form_hide').animate({
							opacity : 1,
							visibility : 'visible',
						},
						{
							duration: 500, 
							complete: function() {
								$(this).slideDown(800);
							}
						});	
					}

				});

			},

			checkBtns: function(){

				var self = this;

				if($('.first_step').hasClass('active')){
					$('.form_left-step').addClass('hidden');
				}
				else{
					$('.form_left-step').removeClass('hidden');	
				}

				if($('.last_step').hasClass('active')){
					$('.form_right-step').addClass('hidden');
				}
				else{
					$('.form_right-step').removeClass('hidden');	
				}

			},

			checkPublication: function(){

				var self = this;
				
				if($(window).width() >= 992){

					if($('.step_box.active').hasClass('full')){
						$('.public_wrap').hide();
					}
					else{
						$('.public_wrap').show();	
					}

				}

			},

			btnSteps: function(){
				
				var self = this;

				$('.form_left-step').on('click', function(){

					var prevStep = $('.form_step_container').find('.step_box.active').last().prevAll('.js-form_wrap');

					$('.form_step_container').find('.step_box.active').last().removeClass('step_box active');
					$(prevStep[0]).addClass('active');

					self.checkBtns();
					self.checkPublication();
					self.anchorForm();
				});

				$('.form_right-step').on('click', function(){

					var nextStep = $('.form_step_container').find('.step_box.active').last().nextAll('.js-form_wrap');

					$('.form_step_container').find('.step_box.active').removeClass('active');
					$(nextStep[0]).addClass('step_box active');

					self.checkBtns();
					self.checkPublication();
					self.anchorForm();
				});

			},

			anchorForm: function(){

				var self = this;
				
				$('html, body').stop().animate({scrollTop: $('.form_step_container').offset().top }, 1500);

			},

			selectChange: function(){

				var self = this;

				self.sel7.on('change',  function() {
					var active = $(this).find('option:selected'),
						box = $('.js-another-org');

					if(active.attr('data-type') == 'another-org'){
						box.show();
					}
					else{
						box.hide();
					}

				});
				
				self.sel6.on('change',  function() {
					var active = $(this).find('option:selected'),
						input = $(this).parents('.form_select').find('.form_input');

					if(active.attr('data-type') == 'other_type'){
						input.removeClass('input_hide');
					}
					else{
						input.addClass('input_hide');
					}

				});
				
				self.sel.on('change',  function() {
					var $selValue = $(this).val(),
						$selIndex = $(this).find('option:selected').index();
					// console.log($selValue,$selIndex)

					if($selIndex ==  '0'){
						// console.log(0)
						$('.primary_publications').removeClass('active');
						$('.recipient').removeClass('active');
					}
					if($selIndex ==  '1'){
						// console.log(1)
						$('.primary_publications').addClass('active');
						$('.recipient').removeClass('active');

						if(self.checkboxPublic.hasClass('checked')){
							$('.reprint').addClass('active');
						}
						else{
							$('.reprint').removeClass('active');
						}
					}
					if($selIndex ==  '2'){
						// console.log(2)
						$('.primary_publications').addClass('active');
						$('.recipient').addClass('active');

						if(self.checkboxPublic.hasClass('checked')){
							$('.reprint').addClass('active');
						}
						else{
							$('.reprint').removeClass('active');
						}
					}

				});

				self.sel2.on('change', function() {

					var $selValue = $(this).val(),
						$selIndex = $(this).find('option:selected').index();
					
					if($selIndex ==  '0'){
						// console.log(0)
						$('.accounting_docs').removeClass('active');
						$('.other_organization').removeClass('active');
						$('.recipient2').removeClass('active');
						setTimeout(function() {
							$('.js-accounting-select2').prop('selectedIndex',0);
							$('.js-accounting-select2').trigger("refresh");
							// console.log(0, $('.js-accounting-select2 option:selected').val())
					    }, 500)
					}
					if($selIndex ==  '1'){
						// console.log(1)
						$('.other_organization').removeClass('active');
						$('.accounting_docs').addClass('active');
						setTimeout(function() {
							$('.js-accounting-select2').prop('selectedIndex',0);
							$('.js-accounting-select2').trigger("refresh");
							// console.log(1, $('.js-accounting-select2 option:selected').val())
					    }, 500)
					}
					if($selIndex ==  '2'){
						// console.log(2)
						$('.other_organization').addClass('active');
						$('.accounting_docs').addClass('active');
						setTimeout(function() {
							$('.js-accounting-select2').prop('selectedIndex',0);
							$('.js-accounting-select2').trigger("refresh");
							// console.log(2, $('.js-accounting-select2 option:selected').val())
					    }, 500)
					}

				});

				self.sel3.on('change', function() {
					var $selValue = $(this).val(),
						$selIndex = $(this).find('option:selected').index();
					
					if($selIndex ==  '0'){
						$('.recipient2').removeClass('active');
					}
					if($selIndex ==  '1'){
						$('.recipient2').removeClass('active');
					}
					if($selIndex ==  '2'){
						$('.recipient2').removeClass('active');
					}
					if($selIndex ==  '3'){
						$('.recipient2').addClass('active');
					}

				});

				self.sel4.on('change',  function() {
					var $selValue = $(this).val(),
						$selIndex = $(this).find('option:selected').index();
					
					if($selIndex ==  '0'){
						$('.re-publish').removeClass('active');
						$('.reprint-Checkbox').show();
					}
					if($selIndex ==  '1'){
						$('.re-publish').addClass('active');
						$('.reprint-Checkbox').hide();
					}

				});

				self.sel5.on('change',  function() {

					if(self.checkToggle.prop('checked')){

						self.checkToggle.trigger('click');
						$('.first_form').find('.form_textarea, .form_input').val('');
						
					}
					

					var $selValue = $(this).val();
					if($selValue ==  '1'){
						$('.reprint-Checkbox').hide();
						$('.publish_first_second').hide();
						$('.organization_created').removeClass('js-form_wrap');
					}
					if($selValue ==  '2'){
						$('.executive_agency').addClass('active');
						$('.additional_documents').addClass('active');
						$('.organization_created').addClass('active js-form_wrap');
						$('.reprint-Checkbox').show();	
						$('.publish_first_second').show();
						textChange('textDefault');
						$('.org_box_item[data-type="1"]').show();
						$('.org_box_item[data-type="2"]').hide();
					}
					else if($selValue ==  '5'){
						$('.organization_created').addClass('active js-form_wrap');
						textChange('text');
						$('.org_box_item[data-type="2"]').show();
						$('.org_box_item[data-type="1"]').hide();
					}
					else{
						$('.executive_agency').removeClass('active');
						$('.additional_documents').removeClass('active');	
						$('.organization_created').removeClass('active');
					}

				});

				function textChange(text){
					$('.form_title_step').each(function(index, el) {
						var $title = $(el).find('.change_title'),
							$textAttr = $title.attr('data-text'),
							$textAttr2 = $title.attr('data-text-default');
						
						if(text == 'text'){
							$title.text($textAttr);
						}
						if(text == 'textDefault'){
							$title.text($textAttr2);
						}
					});
				}

				if(self.sel5.find('option:selected').val() == '1'){
					$('.reprint-Checkbox').hide();
					$('.publish_first_second').hide();
				}

			},

			checkboxEvent: function(){

				var self = this;

				self.checkboxPublic.on('change', function() {

					if($(this).hasClass('checked')) {
						$('.input-day').addClass('active');

						if(self.sel.find('option:selected').index() ==  '1'){
							$('.reprint').addClass('active');
						}
						if(self.sel.find('option:selected').index() ==  '2'){
							$('.reprint').addClass('active');
						}

					} 
					else {
						$('.input-day').removeClass('active');
						$('.reprint').removeClass('active');
					}

				});

			},

			events: function(){

				var self = this;

				$('.js-aff_org_add').on('click', function(){

					$('.js-affiliated_org').show();

					$('.js-affiliated_org_list').find('.org_list_item.editing').removeClass('editing');
					
					$('.js-affiliated_org').find('.afforg_inp').each(function(){
						
						var $this = $(this);
						$this.val('');

					});


				});

				$('.js-aff_org_reset').on('click', function(){

					$('.js-affiliated_org').find('.afforg_inp').each(function(){
						
						var $this = $(this);
						$this.val('');

					});

					$('.js-affiliated_org_list').find('.org_list_item.editing').removeClass('editing');
					
					$('.js-affiliated_org').find('input.error_message').removeClass('error_message');
					
					$('.js-affiliated_org').find('div.error_message').remove();

					$('.js-affiliated_org').hide();

				});

				$('.js-aff_org_save').on('click', function(){

					var org = {};

					$('.afforg_inp').each(function(){

						var $this = $(this),
							$val = $this.val(),
							$name = $this.attr('name');

						org[$name] = $val;

					});

					self.afforgArr.push(org);

					if(!$('.js-affiliated_org_list').find('.org_list_item.editing').length){

						$('.js-affiliated_org_list').append('<div class="org_list_item"><span class="org_list_item_name">'+ org.afforg_opf +'</span><i class="org_list_edit" title="Редактировать"></i></div>');
					}
					else{

						var $item_list = $('.js-affiliated_org_list').find('.org_list_item.editing'),
							$neme = $item_list.find('.org_list_item_name').text();

						$item_list.find('.org_list_item_name').text(org.afforg_opf)

						for (var i = self.afforgArr.length - 1; i >= 0; i--) {
							
							var arrItem = self.afforgArr[i];

							if(arrItem.afforg_opf == $neme){
								
								for (var key in arrItem) {
									arrItem[key] = org[key];
								}

							}
							
						}

						$item_list.removeClass('editing');

					}


					$('.js-affiliated_org').find('.form_input').each(function(){
						
						var $this = $(this);
						$this.val('');

					});

					$('.js-affiliated_org').find('input.error_message').removeClass('error_message');
					
					$('.js-affiliated_org').find('div.error_message').remove();

					$('.js-affiliated_org').hide();

				});

				$('.js-affiliated_org_list').on('click', '.org_list_edit', function(){

					var $this = $(this),
						$neme = $this.siblings('.org_list_item_name').text();

					$('.js-affiliated_org_list').find('.org_list_item.editing').removeClass('editing');
					
					$this.closest('.org_list_item').addClass('editing');

					for (var i = self.afforgArr.length - 1; i >= 0; i--) {
						
						var arrItem = self.afforgArr[i];

						if(arrItem.afforg_opf == $neme){
							
							for (var key in arrItem) {
								$('.js-affiliated_org').find('[name="'+key+'"]').val(arrItem[key]);
							}

						}
						
					}

					$('.js-affiliated_org').show();

				});
			
			}

		},


		/**
		**	openCloseMenu
		**/

		openCloseMenu: function() {

	       	var self = this;

	       	self.w = $(window);


	        $(".js-btn-menu").on('click', function () {

	            $('body, html').toggleClass("action");
	            $(this).toggleClass("active");
	            $(this).siblings("#primary_nav").toggleClass('show');

	        });
		},


		/**
		**	interactiveMap
		**/

		interactiveMap: {

			init: function(){

				var self = this;

				self.regions();
				self.city();
				self.checkRegCity();

			},

			regions: function(){

				var self = this;

				$('.list_regions').on('click', 'li:not(.current)>a', function(event) {
					$(this).parent().addClass('current').closest('li').siblings().removeClass('current');
					$('.list_city li').removeClass('current');
					self.checkRegCity();
				});

			},

			city: function(){

				var self = this;	

				$('.list_city').on('click', 'li:not(.current)>a', function(event) {
					$(this).parent().addClass('current').closest('li').siblings().removeClass('current');
					self.checkRegCity();
				});

			},

			checkRegCity: function(){

				var self = this;	
				if($('.list_regions li.regions1').hasClass('current')){
					$('.list_city').show();
				}
				else{
					$('.list_city').hide();
				}
				if($('.list_regions li.regions1').hasClass('current') && $('.list_city li.city1').hasClass('current')){
					$('.feed_back_wrap').show();
				}
				else{
					$('.feed_back_wrap').hide()
				}

			}

		},

		/**
		**	Yandex Map
		**/

        mapYa: function(){

        	ymaps.ready(init);

    	    var myMap,
    	    	clusterer,
    	    	dataJson,
    	    	markers = [];

    	    function init(){

	    		// Инициализация карты START
		        myMap = new ymaps.Map ("ymap", {
		            center: [54.0746,46.2061],
		            zoom: 6
		        });
		    	// Инициализация карты END
		    	
    	        // Поведение карты
    		    myMap.behaviors
		        	// .disable(['scrollZoom']) //отключает поведение
		        	.enable(['drag', 'dblClickZoom', 'multiTouch']); //включает поведение


    		    // Подгоним размер карты под новый размер контейнера
    			// (например, если изменилась верстка страницы или карта была инициализирована
    			// в скрытом состоянии)
    			myMap.container.fitToViewport();
		    	
		    	var objectManager = new ymaps.ObjectManager({
		            // Чтобы метки начали кластеризоваться, выставляем опцию.
		            clusterize: true,
		            // ObjectManager принимает те же опции, что и кластеризатор.
		            gridSize: 32,
		            clusterDisableClickZoom: true
		        });
				
			    // Чтобы задать опции одиночным объектам и кластерам,
			    // обратимся к дочерним коллекциям ObjectManager.
			    objectManager.objects.options.set('preset', 'islands#greenDotIcon');
			    objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
			    myMap.geoObjects.add(objectManager);

			    $.ajax({
			        url: "data/1.json"
			    }).done(function(data) {

			        objectManager.add(data);
			        dataJson = data;
			        markers = data.features;

			    });

			    $('.list_city>li>a, .list_regions>li>a').on('click', function(){

			    	setTimeout(function(){

				    	if($('.list_regions>.regions1').hasClass('current') && $('.list_city>.city1').hasClass('current')){
				    		objectManager.add(dataJson);
				    	}
				    	else{
				    		for (var i = markers.length - 1; i >= 0; i--) {
				    			
				    			objectManager.remove(markers[i]);
				    		}
				    	}

			    	},300)

			    })

			    $('.js-balloon').on('click', function(){
			    	
			    	$('.js-tabs-list').find('li').eq(1).trigger('click');
			    	myMap.setZoom(10)
			    	myMap.container.fitToViewport();

			    	$('html, body').stop().animate({
			    		scrollTop: $('#ymap').offset().top 
			    	}, 1000);

			    	var id = $(this).attr('data-idBalloon');
			    	for (var i = markers.length - 1; i >= 0; i--) {
			    		if(markers[i].id == id){
			    			myMap.setCenter(markers[i].geometry.coordinates);
			    			objectManager.objects.balloon.open(id);
			    		}
			    	}
			    })

    	    }

        },


		/**
		**	Tabs
		**/

		tabs: function(){

			$('.js-tabs').each(function(){

				var $this = $(this),
					active = $this.find('.js-tabs-list').find("li.active").length ? $this.find('.js-tabs-list').find("li.active") : $this.find('.js-tabs-list').find('li:first-child').addClass('active'),
					li = $this.find('.js-tabs-list').find("li").length,
					index = active.index();

				$this.find('.js-tabs-box').children("div").eq(index).show().addClass('active');

				for (var i = 1; i <= li; i++) {
					$this.find('.js-tabs-dots').append('<div></div>');
				}
				$this.find('.js-tabs-dots').children('div').eq(index).addClass('active').siblings('div').removeClass('active');

			});

			$('.js-tabs-list').on('click', 'li', function(){

				var ind = $(this).index();
				$(this).addClass('active').siblings().removeClass('active');
				$(this).closest('.js-tabs')
					   .find('.js-tabs-box')
					   .children()
					   .eq(ind)
					   .addClass('active')
					   .show()
					   .siblings()
					   .removeClass('active')
					   .hide();

				$(this).closest('.js-tabs').find('.js-tabs-dots').children('div').eq(ind).addClass('active').siblings('div').removeClass('active');

			});

			$('.js-tabs-dots').on('click', 'div', function(){

				var dotsIndex = $(this).index();
				$(this).addClass('active')
					.siblings()
					.removeClass('active')
					.closest('.js-tabs')
					.find('.js-tabs-box')
					.children()
					.eq(dotsIndex)
					.addClass('active')
					.show()
					.siblings()
					.removeClass('active')
					.hide();

				$(this).closest('.js-tabs')
					.find('.js-tabs-list')
					.children()
					.eq(dotsIndex)
					.addClass('active')
					.siblings()
					.removeClass('active');

			});

		},


		/**
		**	Accordion
		**/

		accordion: function(){

			$('.footer_title').on('click', function(event) {
				if($(window).width() <= 576){
					event.preventDefault();
					$(this).toggleClass('active').next('.footer_list').slideToggle(400);
				}
			});	

			$('.js-feed-accordion').on('click', function() {
				if($(window).width() <= 576){
					$(this).toggleClass('active').children('.list_company').slideToggle(400);
				}
			});	

		},


		/**
		**	Close
		**/

		close: function(){
			$('.wary-close').on('click', function(e){
				$(this).closest('.wary_section').slideUp(400);
			});
		},


		/**
		**	Preloader
		**/

		preloader: function(){

			var self = this;

			self.preloader = $('#page-preloader');
	        self.spinner   = self.preloader.find('.preloader');

		    self.spinner.fadeOut();
		    self.preloader.delay(350).fadeOut('slow');
		},

	}


	$(document).ready(function(){

		Core.DOMReady();

	});

	$(window).load(function(){

		Core.windowLoad();

	});

})(jQuery);